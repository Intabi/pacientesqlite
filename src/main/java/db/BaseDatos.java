package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BaseDatos {

    private String database;
    private Connection connection;
    private Statement statement;

    public BaseDatos(String db) throws ClassNotFoundException, SQLException {
        this.database = db;
        this.connection = DriverManager.getConnection("jdbc:sqlite:" + database);
        this.statement = connection.createStatement();
    }

    public Connection getConnection() {
        return connection;
    }

    public List<Usuario> getUsuarioByName(String nombre, String password) throws SQLException {
        ResultSet rs = this.statement.executeQuery("select * from usuario where upper(nombre)='" + nombre.toUpperCase() + "' and password='" + password.toUpperCase() + "'");
        List<Usuario> usuario = new ArrayList();
        while (rs.next()) {
            Usuario temp = new Usuario();
            temp.setIdUsuario(rs.getInt("id_usuario"));
            temp.setIdUsuario(rs.getInt("nombre"));
            temp.setIdUsuario(rs.getInt("password"));
            temp.setIdUsuario(rs.getInt("rol"));
            usuario.add(temp);
        }
        return usuario;
    }

    public boolean addCustomer(Usuario usuario) throws SQLException {
        String sql = "insert into Customer(FirstName, LastName, Company, Address, "
                + "City, State, Country, PostalCode, Phone, Fax, Email, SupportRepId) "
                + "values (?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement prepStmt = this.connection.prepareStatement(sql);
        /*prepStmt.setString(1, customer.getFirstName());
        prepStmt.setString(2, customer.getLastName());
        prepStmt.setString(3, customer.getCompany());
        prepStmt.setString(4, customer.getAddress());
        prepStmt.setString(5, customer.getCity());
        prepStmt.setString(6, customer.getState());
        prepStmt.setString(7, customer.getCountry());
        prepStmt.setString(8, customer.getPostalCode());
        prepStmt.setString(9, customer.getPhone());
        prepStmt.setString(10, customer.getFax());
        prepStmt.setString(11, customer.getEmail());
        prepStmt.setInt(12, customer.getSupportRepId());*/
        return prepStmt.execute();
    }

    public int deleteCustomer(Usuario usuario) throws SQLException {
        String sql = "delete from Customer where CustomerId = ? ";
        PreparedStatement prepStmt = this.connection.prepareStatement(sql);
        /* prepStmt.setInt(1, customer.getCustomerId());*/
        prepStmt.execute();
        return prepStmt.getUpdateCount();
    }

    public int updateCustomer(Usuario usuario) throws SQLException {
        String sql = "update customer set FirstName=?, LastName=?, Company=?, Address=?, City=?, "
                + "State=?, Country=?, PostalCode=?, Phone=?, Fax=?, Email=?, SupportRepId=?"
                + "where CustomerId = ?";
        PreparedStatement prepStmt = this.connection.prepareStatement(sql);
        /*prepStmt.setString(1, customer.getFirstName());
        prepStmt.setString(2, customer.getLastName());
        prepStmt.setString(3, customer.getCompany());
        prepStmt.setString(4, customer.getAddress());
        prepStmt.setString(5, customer.getCity());
        prepStmt.setString(6, customer.getState());
        prepStmt.setString(7, customer.getCountry());
        prepStmt.setString(8, customer.getPostalCode());
        prepStmt.setString(9, customer.getPhone());
        prepStmt.setString(10, customer.getFax());
        prepStmt.setString(11, customer.getEmail());
        prepStmt.setInt(12, customer.getSupportRepId());
        prepStmt.setInt(13, customer.getCustomerId());*/
        prepStmt.execute();
        return prepStmt.getUpdateCount();
    }
}
